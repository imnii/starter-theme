<?php
	/*!
	* Theme Setup
	*/
	if ( ! function_exists( 'starter_theme_setup' ) ) :
		
		function starter_theme_setup() {

			add_theme_support( 'automatic-feed-links' );

			add_theme_support( 'title-tag' );

			add_theme_support( 'post-thumbnails' );

			add_theme_support( 'html5', array(
				'search-form',
				'comment-form',
				'comment-list',
				'gallery',
				'caption',
			) );

			add_theme_support( 'custom-background', apply_filters( 'starter_theme_custom_background_args', array(
				'default-color' => 'ffffff',
				'default-image' => '',
			) ) );

			add_theme_support( 'customize-selective-refresh-widgets' );

			add_theme_support( 'custom-logo', array(
				'height'      => 250,
				'width'       => 250,
				'flex-width'  => true,
				'flex-height' => true,
			) );
		}
	endif;
	add_action( 'after_setup_theme', 'starter_theme_setup' );

	/*!
	* Register widget area.
	*/
	function starter_theme_widgets_init() {
		register_sidebar( array(
			'name'          => esc_html__( 'Sidebar' ),
			'id'            => 'sidebar-1',
			'description'   => esc_html__( 'Add widgets here.' ),
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '<h2 class="widget-title">',
			'after_title'   => '</h2>',
		) );
	}
	add_action( 'widgets_init', 'starter_theme_widgets_init' );

	/*!
	* Theme Scripts
	*/
	function starter_theme_scripts() {

		// Bootstrap 4 Grid + Normalize CSS
		wp_enqueue_style( 'normalize-css', get_template_directory_uri() . '/assets/css/10_normalize.css', array(), null, false );
		wp_enqueue_style( 'bootsrap-4-grid', get_template_directory_uri() . '/assets/css/20_bootstrap4-grid.css', array(), null, false );
		// Slick Slider
		wp_enqueue_style( 'slick-css', get_template_directory_uri() . '/assets/lib/slick-1.8.0/slick/slick.css', array(), null, false );
		// Style CSS + Global CSS + Custom CSS
		wp_enqueue_style( 'style-css', get_stylesheet_uri(), array(), null, false );
		wp_enqueue_style( 'global-css', get_template_directory_uri() . '/assets/css/30_global.css', array(), null, false );
		wp_enqueue_style( 'custom-css', get_template_directory_uri() . '/assets/css/40_custom.css', array(), null, false );
		// JQuery
		wp_deregister_script('jquery');
		wp_register_script('jquery', "https://code.jquery.com/jquery-3.3.1.min.js", array(), null, true );
		wp_enqueue_script('jquery');
		// Slick SLider
		wp_enqueue_script( 'slick-js', get_template_directory_uri() . '/assets/lib/slick-1.8.0/slick/slick.min.js', array(), null, true );
		// Front JS
		wp_enqueue_script( 'front-js', get_template_directory_uri() . '/assets/js/front.js', array(), null, true );

		// PRODUCTION
		// wp_enqueue_style( 'bundle-css', get_template_directory_uri() . '/assets/build/bundle.css', array(), null, false );

		if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
			wp_enqueue_script( 'comment-reply' );
		}

	}
	add_action( 'wp_enqueue_scripts', 'starter_theme_scripts' );

	/*!
	* Admin Scripts
	*/
	function starter_admin_scripts() {
		wp_enqueue_style('admin-styles', get_template_directory_uri().'/assets/admin/admin.css');
	}
	add_action('admin_enqueue_scripts', 'starter_admin_scripts');

	