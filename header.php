<?php
	/*!
	*  Header
	*/
?>

<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="<?php bloginfo( 'charset' ); ?>">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta http-equiv="X-UA-Compatible" content="ie=edge">
		<link rel="profile" href="http://gmpg.org/xfn/11">

		<?php wp_head(); ?>
	</head>
	<body <?php body_class(); ?>>
		<div class="page">
			<header class="site-header">
				<div class="container">
					<nav class="main-navigation">
						<?php
							wp_nav_menu( array(
								'container' => 'Main Navigation',
								'menu' => 'Main Navigation'
							));
						?>
					</nav>
				</div>
			</header>
			<main class="site-content">