<?php
	/*!
	*  Archive
	*/

	get_header();
?>

	<div class="page-archive">
		<section>
			<div class="container">
				<div class="row">
					<div class="col-md-8">
						<?php if ( have_posts() ) : ?>
							<div>
								<?php
									the_archive_title( '<h1>', '</h1>' );
									the_archive_description( '<div>', '</div>' );
								?>
							</div>
							<?php
								while ( have_posts() ) : the_post();
									get_template_part( 'template-parts/content', get_post_type() );
								endwhile;
							?>
						<?php else : ?>
							<?php get_template_part( 'template-parts/content', 'none' ); ?>
						<?php endif; ?>
					</div>
					<div class="col-md-4">
						<?php get_sidebar(); ?>
					</div>
				</div>
			</div>
		</section>
	</div>

<?php get_footer();
