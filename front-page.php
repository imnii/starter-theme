<?php
	/*!
	*  Front page
	*/

	get_header();
?>

    <div class="page-home">
        <section>
            <div class="container">
                <?php if ( have_posts() ) : ?>
                    <?php while ( have_posts() ) :  the_post(); ?>
                        <?php the_content(); ?>
                        <?php 
                            if ( comments_open() || get_comments_number() ) :
                                comments_template();
                            endif;
                        ?>
                    <?php endwhile; ?>
                <?php endif; ?>
            </div>
        </section>
    </div>

<?php get_footer();