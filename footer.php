<?php
	/*!
	*  Footer
	*/
?>
			</main>
			<footer class="site-footer">
                <div class="container text-center">
                    <p>Copyright &copy; <?php echo date('Y'); ?></p>
                </div>
            </footer>
		</div>
		<?php wp_footer(); ?>
	</body>
</html>
