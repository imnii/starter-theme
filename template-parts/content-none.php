<?php
	/*!
	*  Content None
	*/
?>

<div>
	<div>
		<h1><?php esc_html_e( 'Nothing Found'); ?></h1>
	</div>
	<div>
		<?php if ( is_home() && current_user_can( 'publish_posts' ) ) : ?>

			<?php printf(
					'<p>' . wp_kses(
						__( 'Ready to publish your first post? <a href="%1$s">Get started here</a>.'),
						array(
							'a' => array(
								'href' => array(),
							),
						)
					) . '</p>',
					esc_url( admin_url( 'post-new.php' ) )
				);
			?>

		<?php elseif ( is_search() ) : ?>
		
			<p><?php esc_html_e( 'Sorry, but nothing matched your search terms. Please try again with some different keywords.'); ?></p>
			<?php get_search_form(); ?>

		<?php else : ?>

			<p><?php esc_html_e( 'It seems we can&rsquo;t find what you&rsquo;re looking for. Perhaps searching can help.'); ?></p>
			<?php get_search_form(); ?>

		<?php endif; ?>
	</div>
</div>
