<?php
	/*!
	*  Content Page
	*/
?>

<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div>
		<?php the_content(); ?>
	</div>
</div>