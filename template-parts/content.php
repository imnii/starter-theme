<?php
	/*!
	*  Content
	*/
?>

<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div>
		<?php the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' ); ?>
	</div>
	<div>
		<?php the_excerpt(); ?>
	</div>
	<div>
		<div><?php the_date(); ?></div>
	</div>
</div>
