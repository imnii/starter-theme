<?php
	/*!
	*  Content Search
	*/
?>

<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div>
		<div>
			<?php the_title( sprintf( '<h2 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h2>' ); ?>
		</div>	
		<div>
			<?php the_excerpt(); ?>
		</div>
	</div>
</div>
