<?php
	/*!
	*  Index
	*/

	get_header();
?>

	<div class="page-index">
		<section>
			<div class="container">
				<div class="row">
					<div class="col-md-8">
						<?php if ( have_posts() ) : ?>
							<div>
								<h1 class="page-title screen-reader-text"><?php single_post_title(); ?></h1>
							</div>
							<?php
								while ( have_posts() ) : the_post();
									get_template_part( 'template-parts/content', get_post_type() );
								endwhile;
							?>
						<?php else : ?>
								<?php get_template_part( 'template-parts/content', 'none' ); ?>
						<?php endif; ?>
					</div>
					<div class="col-md-4">
						<?php get_sidebar(); ?>
					</div>
				</div>
			</div>
		</section>
	</div>

<?php get_footer();
