<?php
	/*!
	*  Search
	*/

	get_header();
?>

	<div class="page-search">
		<section>
			<div class="container">
				<?php if ( have_posts() ) : ?>
					<header class="page-header">
						<h1 class="page-title">
							<?php
							printf( esc_html__( 'Search Results for: %s' ), '<span>' . get_search_query() . '</span>' );
							?>
						</h1>
					</header>
					<?php
					while ( have_posts() ) :
						the_post();
						get_template_part( 'template-parts/content', 'search' );
					endwhile;
				else :
					get_template_part( 'template-parts/content', 'none' );
				endif;
				?>
			</div>
		</section>
	</div>

<?php get_footer();
