<?php
	/*!
	*  Page
	*/

	get_header();
?>

	<div class="page-default-template">
		<section>
			<div class="container">
				<?php
					while ( have_posts() ) :
						the_post();
						get_template_part( 'template-parts/content', 'page' );
						if ( comments_open() || get_comments_number() ) :
							comments_template();
						endif;
					endwhile;
				?>
			</div>
		</section>
	</div>

<?php get_footer();
