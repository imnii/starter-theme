$(document).ready(function() {
 
    $('.accordion').click(function(){
        $(this).find('.accordion-body').slideToggle('fast');
    });

    $('.slider').slick({
        dots: true,
        prevArrow: '<button type="button" class="slick-prev"><i class="fas fa-chevron-circle-left"></i></button>',
        nextArrow: '<button type="button" class="slick-next"><i class="fas fa-chevron-circle-right"></i></button>'
    });
 
});