<?php
	/*!
	*  404
	*/

	get_header();
?>

	<div class="page-404">
		<section>
			<div class="container">
				<div>
					<h1><?php esc_html_e( 'Oops! That page can&rsquo;t be found.', 'starter-theme' ); ?></h1>
				</div>
				<div>
					<p><?php esc_html_e( 'It looks like nothing was found at this location. Maybe try one of the links below or a search?', 'starter-theme' ); ?></p>
					<?php get_search_form(); ?>
				</div>
			</div>
		</section>
	</div>

<?php get_footer(); 
