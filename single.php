<?php
	/*!
	*  Single
	*/

	get_header();
?>

	<div class="page-single">
		<section>
			<div class="container">
				<?php while ( have_posts() ) : the_post(); ?>
					<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
						<div>
							<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
						</div>
						<div>
							<?php the_content(); ?>
						</div>
						<div>
							<div><?php the_date(); ?></div>
						</div>
					</div>
					<?php
						if ( comments_open() || get_comments_number() ) :
							comments_template();
						endif; 
					?>
				<?php endwhile; ?>
			</div>
		</section>
	</div>

<?php get_footer();
